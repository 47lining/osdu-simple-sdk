import hmac
import hashlib
import base64


def get_secret_hash(username, client_id):
    s = "1fvgjucv8i2rom03pfmqaj2p4fj1b2nt330ij9b26o2fn707jmf6"
    msg = username + client_id
    dig = hmac.new(
        str(s).encode('utf-8'),
        msg=str(msg).encode('utf-8'),
        digestmod=hashlib.sha256
    ).digest()
    return base64.b64encode(dig).decode()
