from urllib.parse import urljoin
from .auth import get_secret_hash

import requests


class APIClientError(Exception):
    message = {"Message": "Internal Server Error."}
    status_code = 500

    def __init__(self, message=None, status_code=None):
        if message is not None:
            self.message = message
        if status_code is not None:
            self.status_code = status_code
        super().__init__(self.message, self.status_code)


class BaseAPIClient:
    exception = APIClientError
    default_region = "us-east-1"
    service_base_url = "https://{}.preview.paas.47lining.com/"
    data_partition_id = None

    @classmethod
    def _get_service_url(cls, stage: str) -> str:
        return cls.service_base_url.format(stage)

    def __init__(self, stage, client_id=None, url=None, headers=None, data_partition_id=None):
        if data_partition_id is not None:
            self.data_partition_id = data_partition_id
        self.stage = stage
        self.client_id = client_id

        if url is not None:
            self.url = url
        else:
            self.url = self._get_service_url(stage)

        if headers is not None:
            self.headers = headers
        else:
            self.headers = {}

    def authorize_with_email_and_password(self, email: str, password: str, data_partition_id=None, client_id=None):
        if data_partition_id is not None:
            self.data_partition_id = data_partition_id

        if client_id is not None:
            self.client_id = client_id

        response = requests.post(
            url=f'https://cognito-idp.{self.default_region}.amazonaws.com/',
            json={
                "ClientId": self.client_id,
                "AuthFlow": "USER_PASSWORD_AUTH",
                "AuthParameters": {
                    "USERNAME": email,
                    "PASSWORD": password,
                    "SECRET_HASH": get_secret_hash(email, self.client_id)
                }
            },
            headers={
                'X-Amz-Target': 'AWSCognitoIdentityProviderService.InitiateAuth',
                'Content-Type': 'application/x-amz-json-1.1'
            }
        )
        if response.status_code // 100 in (4, 5):
            raise self.exception(
                message={
                    "Message": response.json()['message']
                },
                status_code=response.status_code
            )

        headers = {
            'Authorization': f'Bearer {response.json()["AuthenticationResult"]["AccessToken"]}',
            'Data-Partition-Id': self.data_partition_id,

        }
        self.headers = headers

    def _post(self, endpoint, headers={}, json={}, params={}):
        response = requests.post(urljoin(self.url, endpoint), params=params, headers=headers, json=json)
        if response.status_code // 100 in (4, 5):
            raise self.exception(message=response.json(), status_code=response.status_code)

        return response.json()

    def _get(self, endpoint, headers={}, json={}, params={}):
        response = requests.get(urljoin(self.url, endpoint), params=params, headers=headers, json=json)
        if response.status_code // 100 in (4, 5):
            raise self.exception(message=response.json(), status_code=response.status_code)

        return response.json()
