import os
from .base import BaseAPIClient


class OSDUClient(BaseAPIClient):

    def __init__(self, *args, **kwargs):
        BaseAPIClient.__init__(self, *args, **kwargs)

    def _post(self, endpoint, headers={}, json={}, params={}):
        return BaseAPIClient._post(self, endpoint=endpoint, headers=headers, json=json, params=params)

    def _get(self, endpoint, headers={}, json={}, params={}):
        return BaseAPIClient._get(self, endpoint=endpoint, headers=headers, json=json, params=params)

    def get_file_signed_url(self, **kwargs):
        endpoint = os.path.join(self.url, 'api/delivery/v2/GetFileSignedUrl')
        return self._post(endpoint, headers=self.headers, json=kwargs, params={})

    def query(self, **kwargs):
        endpoint = os.path.join(self.url, 'api/search/v2/query')
        return self._post(endpoint, headers=self.headers, json=kwargs, params={})

    def query_with_cursor(self, **kwargs):
        endpoint = os.path.join(self.url, 'api/search/v2/query_with_cursor')
        return self._post(endpoint, headers=self.headers, json=kwargs, params={})

    def find_well_completions_data_for_well_name(self, well_name):
        endpoint = os.path.join(
            self.url,
            f'api/sample-app/v1/find/find_well_completions_data_for_well_name?well_name={well_name}'
        )
        return self._get(endpoint, headers=self.headers)
