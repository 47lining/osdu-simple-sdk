from setuptools import find_packages, setup

setup(
    name='osdu_simple_sdk',
    version='0.1',
    packages=find_packages(exclude=(
        'smoke',
        'tests'
    )),
    install_requires=[
        "requests==2.23.0"
    ],
    include_package_data=True,
)
