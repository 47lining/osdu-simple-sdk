#
# Configuration
#

PROJECT_NAME = osdu-simple-sdk
AWS_REGION ?= us-east-1
BUILD_NUMBER ?= 0

include build/makefile/root.mk
include build/makefile/python.mk
include build/makefile/lint.mk


.PHONY: project_name
project_name:
	@echo -n $(PROJECT_NAME)

